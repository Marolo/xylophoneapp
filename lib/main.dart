import 'package:audiofileplayer/audiofileplayer.dart';
import 'package:flutter/material.dart';

void main() => runApp(XylophoneApp());

class XylophoneApp extends StatelessWidget {
  void playSound(int note) {
    Audio.load('assets/note$note.wav')
      ..play()
      ..dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.black,
        body: SafeArea(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                buildKey(color: Colors.red, sound: 1),
                buildKey(color: Colors.orange, sound: 2),
                buildKey(color: Colors.yellow, sound: 3),
                buildKey(color: Colors.green, sound: 4),
                buildKey(color: Colors.teal, sound: 5),
                buildKey(color: Colors.blue, sound: 6),
                buildKey(color: Colors.purple, sound: 7),
              ]),
        ),
      ),
    );
  }

  Expanded buildKey({Color color, int sound}) {
    return Expanded(
      child: TextButton(
        style: TextButton.styleFrom(
          backgroundColor: color,
        ),
        onPressed: () {
          playSound(sound);
        },
        child: Text(""),
      ),
    );
  }
}
